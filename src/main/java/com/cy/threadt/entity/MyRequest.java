package com.cy.threadt.entity;

import lombok.Data;

import java.util.concurrent.CompletableFuture;

@Data
public class MyRequest {
    private String id;
    private String orderId;
    private CompletableFuture<MyResponse> future;
}
