package com.cy.threadt.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MyResponse {
    private String id;
    private String orderId;
    private BigDecimal money;
}
